﻿using System;
using System.Collections.Generic;

namespace GAF.Operators
{
    class MyMutation : OperatorBase, IGeneticOperator
    {

        private int _evaluations;
        double chance, percentage;
        private static Random rand = new Random();

        public MyMutation(double chance, double procatage)
        {
            this.chance = chance;
            this.percentage = procatage;

            Enabled = true;
            RequiresEvaluatedPopulation = false;
        }
     
        public override int GetOperatorInvokedEvaluations()
        {
            return _evaluations;
        }

        public override void Invoke(Population currentPopulation, ref Population newPopulation, FitnessFunction fitnessFunctionDelegate)
        {
            if (currentPopulation.Solutions == null || currentPopulation.Solutions.Count == 0)
            {
                throw new ArgumentException("There are no Solutions in the current Population.");
            }


            if (newPopulation == null)
            {
                newPopulation = currentPopulation.CreateEmptyCopy();
            }


            CurrentPopulation = currentPopulation;
            NewPopulation = newPopulation;
            FitnessFunction = fitnessFunctionDelegate;

            if (!Enabled)
                return;


            newPopulation.Solutions.Clear();
            newPopulation.Solutions.AddRange(currentPopulation.Solutions);

            var solutionsToProcess = newPopulation.GetNonElites();
    

            _evaluations = 0;
            for(int i=0;i< solutionsToProcess.Count;++i)
            {
                
                Chromosome chromosome = solutionsToProcess[i];
                double tempfit = chromosome.Fitness;
                if (chromosome == null || chromosome.Genes == null)
                {
                    throw new ArgumentException("The Chromosome is either null or the Chromosome's Genes are null.");
                }

                if (chromosome.IsElite)
                {
                    throw new ArgumentException("The Chromosome is Elite.");
                }



                if (chance > rand.NextDouble())
                {
                    
                    chromosome.ClearFitness();
                    int jobsCount = 0;
                    foreach (var e in chromosome.Genes) jobsCount += ((List<int>)(e.ObjectValue)).Count;

                    randomizeReku(chromosome, (int)(jobsCount * percentage));
                    chromosome.Evaluate(fitnessFunctionDelegate);
                    _evaluations++;
                }
            }

        }


        static public Chromosome randomizeReku(Chromosome list, int countOfRemainingToRandomize)
        {
            if (countOfRemainingToRandomize == 0) return list;
            int procesorId = rand.Next(list.Count);
            if (((List<int>)(list.Genes[procesorId].ObjectValue)).Count == 0)
            {
                return randomizeReku(list, countOfRemainingToRandomize - 1);
            }
            int jobId = rand.Next(((List<int>)(list.Genes[procesorId].ObjectValue)).Count);
            int element = ((List<int>)(list.Genes[procesorId].ObjectValue))[jobId];
            ((List<int>)(list.Genes[procesorId].ObjectValue)).RemoveAt(jobId);

            randomizeReku(list, countOfRemainingToRandomize - 1);

            int newProcesorId = rand.Next(list.Count);
            int newJobId = rand.Next(((List<int>)(list.Genes[newProcesorId].ObjectValue)).Count);

            try
            {
                ((List<int>)(list.Genes[newProcesorId].ObjectValue)).Insert(newJobId, element);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(((List<int>)(list.Genes[newProcesorId].ObjectValue)).Count + " " + newJobId);
            }

            return list;
        }
    };
}