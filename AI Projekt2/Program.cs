﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GAF;
using GAF.Operators;

namespace AI_Projekt2
{
    internal class Program
    {
        static private int gensCount = 100;
        static Random rand = new Random();
        static private int p = 4;  //procesory
        static private int j = 20; //prace
        static private int r = 10; //relacje
        static int[,] jobsTime;
        static List<List<int>> relations;
        private static void Main(string[] args)
        {
            const int populationSize = 100;

            var population = new Population();

            getInput();

            //create the chromosomes
            for (var i = 0; i < populationSize; i++)
            {
                List<List<int>> starting = new List<int>[p].ToList();
                for (int j = 0; j < starting.Count; ++j)
                    starting[j] = new List<int>();
                getStartingAssignment(starting);
                var chromosome = ListToChromosome(starting);
                population.Solutions.Add(chromosome);
            }

            //create the elite operator
            var elite = new Elite(5);

            //create the crossover operator
            var myCrossover = new MyCrossover(0.8)
            {
                CrossoverType = CrossoverType.SinglePoint
            };

            var commonSmallSwapMutation = new MyMutation(0.12, 0.05);
            var rareAggressiveSwapMutation = new MyMutation(0.01, 0.2);


            var mem = new Memory(2, 5); 


    
            //create the GA
            var ga = new GeneticAlgorithm(population, CalculateFitness);

            //hook up to some useful events
            ga.OnGenerationComplete += ga_OnGenerationComplete;
            ga.OnRunComplete += ga_OnRunComplete;

            //ga.Operators.Add(elite);
            ga.Operators.Add(mem);

            ga.Operators.Add(commonSmallSwapMutation);
            ga.Operators.Add(rareAggressiveSwapMutation);
         
            //ga.Operators.Add(elite);
            //ga.Operators.Add(mem);
            ga.Operators.Add(myCrossover); 
            //ga.Operators.Add(elite);
            //ga.Operators.Add(crossover);

            //run the GA
            ga.Run(Terminate);

            Console.Read();
        }

        static void ga_OnRunComplete(object sender, GaEventArgs e)
        {
            printBest(e.Population.GetTop(1)[0]);//Console.WriteLine("Generation {1} current = {0}", e.Population.GetTop(1)[0],e.Generation.ToString());
           
        }

        private static void ga_OnGenerationComplete(object sender, GaEventArgs e)
        {
            var fittest = e.Population.GetTop(1)[0];
            var distanceToTravel = calcTime(fittest);
            Console.WriteLine("Generation: {0}, Fitness: {1}, Distance: {2}", e.Generation, fittest.Fitness, distanceToTravel);
        }


        public static double CalculateFitness(Chromosome chromosome)
        {
            var distanceToTravel = calcTime(chromosome);
            if (distanceToTravel == int.MaxValue) return 0;
            return 1 - (double)distanceToTravel / 100;
        }

        public static bool Terminate(Population population, int currentGeneration, long currentEvaluation)
        {
            return currentGeneration > gensCount;
        }

        public static List<List<int>> ChromosomeToList(Chromosome inPut)
        {
            List<List<int>> outPut = new List<int>[inPut.Genes.Count].ToList();
            int i = 0;
            foreach (Gene gene in inPut.Genes)
            {
                outPut[i] = new List<int>();
                foreach (int el in (List<int>)(gene.ObjectValue))
                {
                    outPut[i].Add(el);
                }
                i++;
            }
            return outPut;
        }

        public static Chromosome ListToChromosome(List<List<int>> inPut)
        {
            Chromosome outPut = new Chromosome();
            foreach (List<int> list in inPut)
            {
                List<int> tempList = new List<int>();
                foreach (int el in list)
                {
                    tempList.Add(el);
                }
                outPut.Add(new Gene(tempList));
            }
            return outPut;
        }





        


        static private void getInput(/*out int[,] jobsTime, out List<List<int>> relations*/)
        {
            using (StreamReader file = new StreamReader("jobs.txt"))
            {
                jobsTime = new int[j, p];
                for (int i = 0; i < j; ++i)
                    for (int k = 0; k < p; ++k)
                    {
                        jobsTime[i, k] = int.Parse(file.ReadLine()); //scanInt();
                    }
            }

            using (StreamReader file = new StreamReader("relations.txt"))
            {
                relations = new List<List<int>>();
                for (int i = 0; i < j; ++i)
                    relations.Add(new List<int>());
                for (int i = 0; i < r; i++)
                {
                    int temp = int.Parse(file.ReadLine());//scanInt();
                    relations[int.Parse(file.ReadLine())/*scanInt()*/].Add(temp);
                }
            }

            
        }
    
        
        static void printBest(Chromosome h)
        {
            List<List<int>> bestAssignment = ChromosomeToList(h);
            Console.WriteLine();
            Console.WriteLine("best = {0}", calcTime(h));
            Console.WriteLine();
            for (int i = 0; i < p; ++i)
            {
                Console.WriteLine("Processorowi {0} przypisano zadania: ", i);
                foreach (var job in bestAssignment[i])
                {
                    Console.Write(job + ",");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("dla tablicy czasow:");
            Console.WriteLine();
            Console.WriteLine("       numer procesora");
            Console.WriteLine("          |0|1|2|3|");
            Console.WriteLine("----------|-|-|-|-|");

            for (int i = 0; i < j; ++i)
            {
                Console.Write("Praca " + i.ToString("D3") + " |");
                for (int k = 0; k < p; ++k)
                {
                    Console.Write(jobsTime[i, k] + "|");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("I tablicy zaleznosci:");
            for (int i = 0; i < relations.Count(); ++i)
            {
                if (relations[i].Count() == 0) continue;
                if (relations[i].Count() == 1)
                    Console.Write("Przed zadaniem {0} należy wykonać zadanie: ", i);
                else
                    Console.Write("Przed zadaniem {0} należy wykonać zadania: ", i);
                for (int k = 0; k < relations[i].Count(); ++k)
                {
                    Console.Write("{0},", relations[i][k]);
                }
                Console.WriteLine();
            }
        }
        


        static private void getStartingAssignment(List<List<int>> list)
        {
            for (int i = 0; i < j; ++i)
            {
                int procesor = rand.Next(list.Count); ;
                int job = rand.Next(list[procesor].Count);
                list[procesor].Insert(job, i);
            }
            while(calcTime(ListToChromosome(list)) == int.MaxValue)
            {
                randomizeReku(list,list.Count/2);
            }
        }
        



        


        


        static private int calcTime(Chromosome h /*List<List<int>> dep, int[,] jobsTime*/)
        {

            List<List<int>> Assigments = ChromosomeToList(h);

            //assert(Assigments.size() == j);

            int SumTime = 0;

            List<bool> czyUkonczone = new bool[j + 1].ToList(); //1 dodatkowy na init
            List<bool> czyProcesorSkonczyl = new bool[p].ToList();
            List<int> tablica_czasu = new int[p].ToList();
            List<Queue<int>> jobsByProcessor = new Queue<int>[p].ToList();//= Assigments.Select(e => (new int[] { j }.ToList()).Concat(e).ToList()  )).Select(e => new Queue<int>(e)).ToList();// new List<Queue<int>>(r);
            for (int i = 0; i < p; ++i)
                jobsByProcessor[i] = new Queue<int>();



            for (int i = 0; i < p; ++i)
            {
                jobsByProcessor[i].Enqueue(j);
                foreach (var item in Assigments[i])
                {
                    jobsByProcessor[i].Enqueue(item);
                }
            }





            while (czyProcesorSkonczyl.Contains(false))
            {

                for (int i = 0; i < p; ++i)
                {

                    if (tablica_czasu[i] == 0 && !czyProcesorSkonczyl[i])
                    {

                        czyUkonczone[jobsByProcessor[i].Dequeue()] = true;


                        if (jobsByProcessor[i].Count() == 0)
                        {
                            czyProcesorSkonczyl[i] = true;
                            continue;
                        }
                    }
                    //}
                }
                //przydzial nowych zadan
                for (int i = 0; i < p; ++i)
                {
                    if (!czyProcesorSkonczyl[i])
                    {
                        if (tablica_czasu[i] <= 0)
                        {
                            if (sprawdzCzyMogeWziac(jobsByProcessor[i].Peek(), relations, czyUkonczone))
                            {
                                tablica_czasu[i] = jobsTime[jobsByProcessor[i].Peek(), i];
                            }
                        }
                    }
                }


                //przesuniecie w czasie.

                int min = tablica_czasu.Select(a => a <= 0 ? int.MaxValue : a).Min();
                if (min == int.MaxValue)
                {
                    if (czyProcesorSkonczyl.Contains(false))
                    {
                        return int.MaxValue;
                    }
                    else
                    {
                        return SumTime;
                    }
                }

                for (int i = 0; i < p; ++i)
                {
                    tablica_czasu[i] -= min;
                }
                SumTime += min;
            }
            return SumTime;
        }
        static bool sprawdzCzyMogeWziac(int id, List<List<int>> dep, List<bool> ukonczone)
        {
            bool czymoge = true;
            foreach (var v in dep[id])
                if (ukonczone[v] == false)
                    return false;

            return czymoge;
        }



        static private List<List<int>> randomizeReku(List<List<int>> list, int heat)
        {
            if (heat == 0) return list;
            int procesor = rand.Next(list.Count);
            if (list[procesor].Count == 0)
            {
                return randomizeReku(list, heat - 1);
            }
            int job = rand.Next(list[procesor].Count);
            int element = list[procesor][job];
            list[procesor].RemoveAt(job);

            randomizeReku(list, heat - 1);

            int newProcesor = rand.Next(list.Count);
            int newJob = rand.Next(list[newProcesor].Count);
            

            try
            {
                list[newProcesor].Insert(newJob, element);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(list[newProcesor].Count + " " + newJob);
            }

            return list;
        }


    }
}


