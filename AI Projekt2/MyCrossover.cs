﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GAF.Operators
{
    public class MyCrossover : Crossover
    {
        #region useless
        internal MyCrossover()
            : this(1.0)
        {
        }
        public MyCrossover(double crossOverProbability)
            : this(crossOverProbability, true)
        {
        }
        public MyCrossover(double crossOverProbability, bool allowDuplicates)
            : this(crossOverProbability, allowDuplicates, CrossoverType.SinglePoint)
        {
        }
        public MyCrossover(double crossOverProbability, bool allowDuplicates, CrossoverType crossoverType)
            : this(crossOverProbability, allowDuplicates, crossoverType, Operators.ReplacementMethod.GenerationalReplacement)
        {
        }

        public MyCrossover(double crossOverProbability, bool allowDuplicates, CrossoverType crossoverType, ReplacementMethod replacementMethod)
            : base(crossOverProbability, allowDuplicates, crossoverType, replacementMethod)
        {
        }
        #endregion
        protected override void PerformCrossoverSinglePoint(Chromosome p1, Chromosome p2, CrossoverData crossoverData, out List<Gene> cg1, out List<Gene> cg2)
        {
            if (crossoverData.Points == null || crossoverData.Points.Count < 1)
            {
                throw new ArgumentException(
                    "The CrossoverData.Points property is either null or is missing the required crossover points.");
            }

            //moja interpretacja:
            /*
			wezme polowe elementow z jednej listy,
			uzupelnij reszta drugiej
			*/

            List<List<int>> p1l = p1.Genes.Select(c => (List<int>)c.ObjectValue).ToList();
            List<List<int>> p2l = p2.Genes.Select(c => (List<int>)c.ObjectValue).ToList();
            if (p1l.Count() != p2l.Count())
            {
                Console.WriteLine("diff counts of gens");
            }
            List<List<int>> newl1 = (new List<int>[4]).Select(e => new List<int>()).ToList();
            List<List<int>> newl2 = (new List<int>[4]).Select(e => new List<int>()).ToList();

            for (int i = 0; i < p1l.Count; ++i)
            {
                for (int j = 0; j < p1l[i].Count / 2; ++j)
                {
                    newl1[i].Add(p1l[i][j]);
                }
                for (int j = 0; j < p2l[i].Count / 2; ++j)
                {
                    newl2[i].Add(p2l[i][j]);
                }
            }

            //uzupelnianie
            List<int> wykorzystane1 = new List<int>();
            List<int> wykorzystane2 = new List<int>();
            foreach (var e in newl1)
                wykorzystane1.AddRange(e);
            foreach (var e in newl2)
                wykorzystane2.AddRange(e);

            for (int i = 0; i < p2l.Count; ++i)
            {
                for (int j = 0; j < p2l[i].Count; ++j)
                {
                    if (!wykorzystane1.Contains(p2l[i][j]))
                        newl1[i].Add(p2l[i][j]);
                }
                for (int j = 0; j < p1l[i].Count; ++j)
                {
                    if (!wykorzystane2.Contains(p1l[i][j]))
                        newl2[i].Add(p1l[i][j]);
                }
            }

            cg1 = newl1.Select(e => new Gene(e)).ToList();
            cg2 = newl1.Select(e => new Gene(e)).ToList();


        }
    };
}
